﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication4.Controllers
{
    public class TestController : Controller
    {

        // GET: Test
        public ActionResult Index()
        {
            ViewData["aaa"] = "111";
            ViewBag.Name = "vita";

            ViewData["A"] = 1;
            ViewData["B"] = 2;

            ViewBag.A = 1;
            ViewBag.B = 2;

            return View();
        }
        
        public ActionResult Html()
        {
            return View();
        }

        public ActionResult HtmlHelper()
        {
            return View();
        }

        public ActionResult Razor()
        {
            return View();
        }
    }
}
