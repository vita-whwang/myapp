﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication4.Models
{
    [MetadataType(typeof(UsersMetadata))]
    public partial class Users
    {
    }

    public class UsersMetadata
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="必填欄位")]
        [DisplayName("姓名")]
        [StringLength(10)]
        public string Name { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("電子郵件")]
        [StringLength(50)]
        [EmailAddress]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("密碼")]
        [StringLength(10)]
        public string Password { get; set; }

        [DisplayName("生日")]
        [Required(ErrorMessage = "必填欄位")]
        public System.DateTime Birthday { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("性別")]
        public bool Gender { get; set; }
    }
}