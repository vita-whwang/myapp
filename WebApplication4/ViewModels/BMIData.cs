﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication4.ViewModels
{
    public class BMIData
    {
        [Display(Name = "身高")]
        [Required(ErrorMessage = "必填欄位12313")]
        [Range(50, 200, ErrorMessage = "請輸入50-200的數值")]
        public float? Height { get; set; }

        [Display(Name = "體重")]
        [Required(ErrorMessage = "必填欄位456465")]
        [Range(30, 150, ErrorMessage = "請輸入30-150的數值")]
        public float? Weight { get; set; }

        public float? Result { get; set; }

        public String Level { get; set; }
    }
}